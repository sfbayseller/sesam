package de.larcado.sesam;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.larcado.sesam.model.Domain;
import de.larcado.sesam.model.JsonStorage;
import de.larcado.sesam.model.Profil;
import de.larcado.sesam.utils.AppDialog;

public class MyDomainsActivity extends AppCompatActivity {

    private static final Pattern MIN_EIN_ZEICHEN = Pattern.compile(".+");

    private JsonStorage jsonStorage;
    private RecyclerView recyclerView;
    private PasswordAdapter mAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mydomains);

        jsonStorage = JsonStorage.load(this);

        recyclerView = findViewById(R.id.recycler_view);

        mAdapter = new PasswordAdapter(this, jsonStorage, jsonStorage.getDomains(),
                domain -> {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppDialogTheme);
                    builder.setTitle("Edit entry");
                    LayoutInflater inflater = this.getLayoutInflater();
                    View dialogView = inflater.inflate(R.layout.dialog_domain, null);
                    EditText editText = dialogView.findViewById(R.id.name);
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                        }

                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            Matcher matcher = MIN_EIN_ZEICHEN.matcher(s.toString());
                            if (!matcher.matches()) {
                                editText.setError("This entry must conatin at least one character");
                            } else {
                                editText.setError(null);
                            }
                        }
                    });
                    editText.setText(domain.getName());
                    Spinner profilSpinner = dialogView.findViewById(R.id.profil);
                    ArrayAdapter<Profil> spinnerAdapter = new ArrayAdapter<>(
                            this,
                            android.R.layout.simple_spinner_dropdown_item,
                            jsonStorage.getProfile()
                    );
                    profilSpinner.setAdapter(spinnerAdapter);
                    builder.setView(dialogView);
                    builder.setPositiveButton("OK", (dialog, which) -> {
                        domain.setName(editText.getText().toString());
                        domain.setProfilID(((Profil) profilSpinner.getSelectedItem()).getId());
                        jsonStorage.save(this);
                        Toast.makeText(this, domain.getName() + " changed", Toast.LENGTH_LONG).show();
                        refresh();
                        dialog.dismiss();
                    });
                    builder.show();
                    for (int i = 0; i < profilSpinner.getCount(); i++) {
                        Profil p = (Profil) profilSpinner.getItemAtPosition(i);
                        if (domain.getProfilID() == p.getId()) {
                            profilSpinner.setSelection(i, true);
                            return;
                        }
                    }
                }, domain -> {
            AppDialog.yesNoAlertDialog(this,
                    "Delete?",
                    "Do you want to delete this entry?",
                    (dialog, which) -> {
                        jsonStorage.getDomains().remove(domain);
                        jsonStorage.save(this);
                        Toast.makeText(this, domain.getName() + " deleted", Toast.LENGTH_LONG).show();
                        refresh();
                    }, null
            );
        });
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        refresh();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppDialogTheme);
            builder.setTitle("New entry");
            LayoutInflater inflater = this.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.dialog_domain, null);
            EditText editText = dialogView.findViewById(R.id.name);
            editText.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    Matcher matcher = MIN_EIN_ZEICHEN.matcher(s.toString());
                    if (!matcher.matches()) {
                        editText.setError("This entry must conatin at least one character");
                    } else {
                        editText.setError(null);
                    }
                }
            });
            Spinner profilSpinner = dialogView.findViewById(R.id.profil);
            ArrayAdapter<Profil> spinnerAdapter = new ArrayAdapter<>(
                    this,
                    android.R.layout.simple_spinner_dropdown_item,
                    jsonStorage.getProfile()
            );
            profilSpinner.setAdapter(spinnerAdapter);
            builder.setView(dialogView);
            builder.setPositiveButton("OK", (dialog, which) -> {
                Domain domain = new Domain();
                domain.setName(editText.getText().toString());
                domain.setProfilID(((Profil) profilSpinner.getSelectedItem()).getId());
                jsonStorage.getDomains().add(domain);
                Collections.sort(jsonStorage.getDomains());
                jsonStorage.save(this);
                Toast.makeText(this, domain.getName() + " added", Toast.LENGTH_LONG).show();
                refresh();
                dialog.dismiss();
            });
            builder.show();
        });
    }

    private void refresh() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_liste) {
            Intent intent = new Intent(this, MyProfileActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
