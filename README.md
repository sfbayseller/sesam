# sesam

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/app/de.larcado.sesam)

A password manager app that does not store the passwords but only generates them based on a salted hash function.

<img src="https://gitlab.com/larcado/sesam/raw/master/screenshots/main_1.png" width="360" height="640">
<img src="https://gitlab.com/larcado/sesam/raw/master/screenshots/main_2.png" width="360" height="640">

## How does this work?

It's quite simple. You remember a **master password**. It is important that nobody knows it, so keep it secure :D
Second, there is a **domain** to which you want to generate a password.

### Step 1
Simple string concatenation
> **base** = domain + master password

### Step 2
The Hash-Algorithm **PBKDF2WithHmacSHA1** will be used alongside a user pre defined **salt**.
By default, a 256-bit hash is created from the **base** with 32,000 iterations.

### Step 3
The generated 256-bit hash will be interpreted as a **BigInteger**.
Now there are *(pre defined by user settings)*:
> **allowedCharacter** = lowerCaseLetters + upperCaseLetters + numbers + specialCharacters

The BigInteger is calculated modulo the number of allowed characters.  This remainder corresponds to the xth character of the **allowedCharacters** (First character of the password).
Now the BigIntger is divided by the desired length of the resulting password.
This process is repeated until all characters have been determined.

## App-Features

### Quick settings tile
Quick access to password manager with quick setting tile. Its the key on the right side.

<img src="https://gitlab.com/larcado/sesam/raw/master/screenshots/quick_settings_1.png" width="360" height="640">
<img src="https://gitlab.com/larcado/sesam/raw/master/screenshots/quick_settings_2.png" width="360" height="640">


### Copy to Clipboard with Notification
If you click on the password, it will be loaded into the clipboard. A Notification appears. Now you can paste it e.g. in the browser in a password field. As soon as the notification is wiped away, the clipboard is cleared.

<img src="https://gitlab.com/larcado/sesam/raw/master/screenshots/notification.png" width="360" height="640">

### Support for Oreo's Autofill API
Click on a password field for another app and choose "autofill with sesam". You will be added to an activity that will allow you to generate the required password.
This is automatically used as input for the long-clicked password field.

### Define password profiles
A password profile is a schema according to which a password should be established. This includes the **length of a password** and which **special characters** are allowed.

Often there are restrictions of operators which may be contained in a password.